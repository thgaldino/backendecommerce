const jwt = require("jsonwebtoken");
const { JWT_SECRET } = require("../config/keys");

module.exports = {
  auth: async (req, res, next) => {
    const bearerToken = req.header("authorization");
    if (!bearerToken)
      return res.status(401).send("Acesso negado. Não possui Token.");

    try {
      if (typeof bearerToken !== "undefined") {
        const bearer = bearerToken.split(" ");
        const token = bearer[1];
        const decoded = jwt.verify(token, JWT_SECRET);

        req.user = decoded;
        await next();
      } else {
        res.status(400).send("Token invalido.");
      }
    } catch (err) {
      res.status(400).send("Token invalidos.");
    }
  },
  admin: async (req, res, next) => {
    // 401 Unauthorized
    // 403 Forbidden

    if (!req.user.isAdmin || req.user.role !== "admin")
      return res.status(403).send("Acesso negado.");

    await next();
  },
};
