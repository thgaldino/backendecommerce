# E-Commerce API

API para utlização em E-commerce

### Tecnologias utilizadas

Express - mongodb - jwt

### Link para Heroku

# EndPoints

## auth

### POST REGISTER

/api/auth/register
email: {
type: String,
minlength: 5,
maxlength: 255,
unique: true,
lowercase: true,
required: true,
},
password: {
type: String,
minlength: 8,
maxlength: 1024,
required: true,
},
confirmPassword: {
type: String,
minlength: 8,
maxlength: 1024,
required: true,
}

### POST LOGIN

/api/auth/login
email: {
type: String,
minlength: 5,
maxlength: 255,
unique: true,
lowercase: true,
required: true,
},
password: {
type: String,
minlength: 8,
maxlength: 1024,
required: true,
}
