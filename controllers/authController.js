const { User } = require("../models/User");
const asyncMiddleware = require("../middlewares/async");

module.exports = {
  register: asyncMiddleware(async (req, res) => {
    const { email, password, confirmPassword } = req.body;
    if (password !== confirmPassword) {
      return res
        .status(400)
        .json(`"invalid" Senha e confirmação de senha não são iguais`);
    }

    const foundUser = await User.findOne({ email });
    if (foundUser) {
      return res.status(400).json(`"exist" Usuário já registado.`);
    }

    const newUser = new User({
      email,
      password,
    });

    const user = await newUser.save();

    const token = `Bearer ${user.generateAuthToken()}`;

    res.header("authorization", token).status(200).json({ token });
  }),
  login: asyncMiddleware(async (req, res) => {
    const { email, password } = req.body;
    let user = await User.findOne({ email });
    if (!user) return res.status(400).json(`"invalid" email ou Senha.`);

    const isValidPassword = await user.isValidPassword(password);
    if (!isValidPassword)
      return res.status(400).json(`"invalid" email ou Senha.`);

    const token = `Bearer ${user.generateAuthToken()}`;
    res.header("authorization", token).status(200).json({ token });
  }),
  secret: async (req, res) => {
    res.status(200).json({ message: "pagina secreta!!" });
  },
};
