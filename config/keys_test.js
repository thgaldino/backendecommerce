require("dotenv/config");

module.exports = {
  PORT: 5554,
  MONGO_URI: process.env.MONGO_URI,
  JWT_SECRET: "secret",
  JWT_SECRET_CART: "cartsecret",
};
