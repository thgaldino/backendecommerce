require("dotenv/config");

if (process.env.NODE_ENV === "production") {
  console.log("PRODUCTION ENV");
  module.exports = require("./keys_prod");
} else if (process.env.NODE_ENV === "test") {
  console.log("TEST ENV");
  module.exports = require("./keys_test");
} else {
  console.log("DEV ENV");
  module.exports = require("./keys-dev");
}
